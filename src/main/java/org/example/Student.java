package org.example;

public class Student extends Person {
    private double math;
    private double physics;
    private double chemistry;
    private double average;

    public Student(int id, String name, int age, String address, double math, double physics, double chemistry) {
        super(id, name, age, address);
        this.math = math;
        this.physics = physics;
        this.chemistry = chemistry;
        this.average = (math + physics + chemistry)/3;
    }

    public Student(double math, double physics, double chemistry, double average) {
        this.math = math;
        this.physics = physics;
        this.chemistry = chemistry;
        this.average = (math + physics + chemistry)/3;
    }

    public double getAverage() {
        return average;
    }

    public double getMath() {
        return math;
    }

    public void display() {
//        System.out.println("Id:"+getId());
//        System.out.println("Tên:"+getName());
        super.display();
        System.out.println("Toán: "+getMath());
        System.out.println("Vật lý: "+physics);
        System.out.println("Hóa: "+chemistry);
        System.out.println("TB: "+getAverage());
        System.out.println("---------------------");
    }
}
