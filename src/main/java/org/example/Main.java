package org.example;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Student> studentList = new ArrayList<>();
        List<Student> studentOlderThan23 = new ArrayList<>();
        List<Student> studentWithHoang = new ArrayList<>();
        List<Student> studentLiveInHanoi = new ArrayList<>();
        List<Student> studentWithHighestAverage = new ArrayList<>();


        while (true) {
            System.out.println("Nhập Id học sinh:");
            int id = Integer.parseInt(scanner.nextLine());

            System.out.println("Nhập tên học sinh:");
            String ten = scanner.nextLine();

            System.out.println("Nhập địa chỉ học sinh:");
            String diachi = scanner.nextLine();

            System.out.println("Nhập điểm toán:");
            double math = Double.parseDouble(scanner.nextLine());

            System.out.println("Nhập điểm vật lý:");
            double physic = Double.parseDouble(scanner.nextLine());

            System.out.println("Nhập điểm hóa:");
            double chemical = Double.parseDouble(scanner.nextLine());

            Student student = new Student(id, ten, 16, diachi, math, physic, chemical);
            studentList.add(student);

            System.out.println("Bạn có muốn nhập thông tin của học sinh khác không? (y/n)");
            String input = scanner.nextLine();

            if (!input.equalsIgnoreCase("y")) {
                break;
            }
        }

            System.out.println("Danh sách học sinh:");
            for (Student student : studentList) {
                student.display();
                if (student.getAge() > 23) {
                    studentOlderThan23.add(student);
                }
                if (student.getName().startsWith("Hoàng")) {
                    studentWithHoang.add(student);
                }
                if (student.getAddress().startsWith("Ha Noi")) {
                    studentLiveInHanoi.add(student);
                }
            }

        System.out.println("Học sinh có độ tuổi trên 23:");
        for (Student student : studentOlderThan23) {
            System.out.println(student.getName());
        }

        System.out.println("Học sinh có tên họ là Hoàng:");
        for (Student student : studentWithHoang) {
            System.out.println(student.getName());
        }

        System.out.println("Học sinh có địa chỉ ở Hà Nội:");
        for (Student student : studentLiveInHanoi) {
            System.out.println(student.getName());
        }

        Collections.sort(studentList, new Comparator<Student>() {
            @Override
            public int compare(Student s1, Student s2) {
                return Double.compare(s2.getAverage(),  s1.getAverage());
            }
        });

        // Display the sorted list
        System.out.println("Danh sách học sinh theo điểm trung bình từ cao đến thấp:");
        for (Student student : studentList) {
            student.display();
        }
    }
}